#include "ErrorInfo.h"
#include "FileObject.h"

Removal::FileObject::FileObject(const std::wstring& name)
{
	fileHandle = CreateFileW(name.c_str(), GENERIC_READ | GENERIC_WRITE,
		0, nullptr, OPEN_EXISTING, 0, NULL);
	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		throw ErrorInfo::MakeLastError();
	}
}

Removal::FileObject::~FileObject()
{
	CloseHandle(fileHandle);
}

HANDLE Removal::FileObject::GetHandle() const noexcept
{
	return fileHandle;
}
