#pragma once
#include <cstdint>
#include <vector>

class RandomGenerator
{
public:
	static std::vector<uint8_t> CreateRandomData(uint32_t size);

	RandomGenerator() = delete;
	~RandomGenerator() = delete;
	RandomGenerator& operator=(const RandomGenerator&) = delete;
	RandomGenerator& operator=(RandomGenerator&&) = delete;
	RandomGenerator(const RandomGenerator&) = delete;
	RandomGenerator(RandomGenerator&&) = delete;
};

