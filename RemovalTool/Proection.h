#pragma once
#include <atomic>
#include <cstdint>
#include <functional>
#include <Windows.h>

#include "MapObject.h"

namespace Removal
{
	static uint32_t GetPageSize()
	{
		static std::atomic<uint32_t> pageSize{ 0 };
		if (!pageSize)
		{
			SYSTEM_INFO sysinfo = { 0 };
			::GetSystemInfo(&sysinfo);
			pageSize = sysinfo.dwAllocationGranularity;
		}
		return pageSize;
	}

	class Proection
	{
		class Iterator
		{
		public:
			explicit Iterator(uint64_t position = 0);				
			Iterator(const Iterator&) = default;
			Iterator(Iterator&&) = default;
			Iterator& operator=(const Iterator&) = default;
			Iterator& operator=(Iterator&&) = default;
			uint64_t GetPosition() const noexcept;			
			bool operator<(const Iterator& second);
			Iterator& operator++();

		private:
			uint64_t fileOffset;
		};
	public:
		explicit Proection(const MapObject& filemap);
		void DestroyContent();
		Proection() = delete;
		~Proection() = default;
		Proection(const Proection&) = delete;
		Proection(Proection&&) = delete;
		Proection& operator=(const Proection&) = delete;
		Proection& operator=(Proection&&) = delete;

	private:
		const MapObject& map;
		Iterator start;
		Iterator stop;

		Iterator begin() const;
		Iterator end() const;
		void OverwriteWithZero(uint64_t start);
		void OverwriteWithFF(uint64_t start);
		void OverwriteWithRandom(uint64_t start);
		void Overwrite(uint64_t start, 
			std::function<void(uint8_t* data, uint32_t size)> overwriteimpl);
	};
}
