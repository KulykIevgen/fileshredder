#include "Eraser.h"
#include "RandomGenerator.h"
#include "ErrorInfo.h"

#include <iostream>
#include <shlwapi.h>
#pragma comment(lib,"Shlwapi.lib")

Removal::Eraser::Eraser(const std::wstring& name):
	filepath(name)
{
	fileObject.reset(new FileObject(name));
	mapObject.reset(new MapObject(*fileObject));
	proection.reset(new Proection(*mapObject));

	sectorClusterOffset = GetFileLocation(fileObject->GetHandle());
}

void Removal::Eraser::EraseContent()
{
	proection->DestroyContent();
}

void Removal::Eraser::Destroy(const std::wstring& name) try
{
	auto newName = Eraser::EraseName(name);
	{
		Eraser eraser(newName);
		auto location = eraser.GetLocation();
		eraser.EraseContent();
		if (location != eraser.GetLocation())
		{
			std::cout << "Old location: " << location << " , new location: " <<
				eraser.GetLocation() << std::endl;
		}
	}	
	Eraser::DeleteFromDisk(newName);
}
catch (const std::exception& error)
{
	std::cout << error.what() << std::endl;
}

uint64_t Removal::Eraser::GetLocation() const
{
	return GetFileLocation(fileObject->GetHandle());
}

uint64_t Removal::Eraser::GetFileLocation(HANDLE fileHandle) const
{
	STARTING_VCN_INPUT_BUFFER  vcn_buffer{ 0 };
	RETRIEVAL_POINTERS_BUFFER  retrieval_buffer{ 0 };
	DWORD retbytes = 0;

	if (!DeviceIoControl(fileHandle, FSCTL_GET_RETRIEVAL_POINTERS,
		&vcn_buffer, sizeof(vcn_buffer),
		&retrieval_buffer, sizeof(retrieval_buffer), &retbytes, nullptr))
	{
		throw ErrorInfo::MakeLastError();
	}

	return static_cast<uint64_t> (retrieval_buffer.Extents[0].Lcn.QuadPart);
}

std::wstring Removal::Eraser::EraseName(const std::wstring& name)
{
	const char Alphabet[]{ 'a','b','c','d' };

	std::wstring fileName = PathFindFileNameW(name.c_str());

	auto data = RandomGenerator::CreateRandomData(static_cast<uint32_t> (fileName.length()));
	for (auto& x : data)
	{
		x = Alphabet[x % sizeof(Alphabet)];
	}	
	std::wstring path = name.substr(0, name.length() - fileName.length());
	std::wstring result = path + L"\\" + std::wstring(data.cbegin(), data.cend());
	
	if (!MoveFileW(name.c_str(), result.c_str()))
	{
		throw ErrorInfo::MakeLastError();
	}	

	return result;
}

void Removal::Eraser::DeleteFromDisk(const std::wstring& name)
{
	if (!DeleteFileW(name.c_str()))
	{
		throw ErrorInfo::MakeLastError();
	}
}
