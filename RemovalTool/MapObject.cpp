#include "ErrorInfo.h"
#include "MapObject.h"

Removal::MapObject::MapObject(const FileObject& FileHandle)
{
	LARGE_INTEGER filesize{ 0 };
	::GetFileSizeEx(FileHandle.GetHandle(), &filesize);
	fileparts = filesize.QuadPart;	

	MapHandle = ::CreateFileMappingW(FileHandle.GetHandle(), nullptr,
		PAGE_READWRITE, 0, 0, nullptr);
	if (MapHandle == INVALID_HANDLE_VALUE)
	{
		throw ErrorInfo::MakeLastError();
	}
}

Removal::MapObject::~MapObject()
{
	::CloseHandle(MapHandle);
}

HANDLE Removal::MapObject::GetHandle() const noexcept
{
	return MapHandle;
}

uint64_t Removal::MapObject::GetSize() const noexcept
{
	return fileparts;
}
