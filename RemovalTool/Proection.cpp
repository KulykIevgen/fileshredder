#include "Proection.h"
#include "ErrorInfo.h"
#include "RandomGenerator.h"
#ifdef _DEBUG
#include <iostream>
#endif

Removal::Proection::Iterator::Iterator(uint64_t position):
	fileOffset(position)
{

}

uint64_t Removal::Proection::Iterator::GetPosition() const noexcept
{
	return fileOffset;
}

bool Removal::Proection::Iterator::operator<(const Iterator& second)
{
	return this->fileOffset < second.fileOffset;
}

Removal::Proection::Iterator& Removal::Proection::Iterator::operator++()
{
	fileOffset += GetPageSize();
	return *this;
}

Removal::Proection::Proection(const MapObject& filemap):
	map(filemap)
{
	start = Iterator{ 0 };
	stop = Iterator(map.GetSize());
}

void Removal::Proection::DestroyContent()
{
	for (auto it = begin(); it < end(); ++it)
	{
		OverwriteWithZero(it.GetPosition());
		OverwriteWithFF(it.GetPosition());
		OverwriteWithRandom(it.GetPosition());
	}
}

Removal::Proection::Iterator Removal::Proection::begin() const
{
	return start;
}

Removal::Proection::Iterator Removal::Proection::end() const
{
	return stop;
}

void Removal::Proection::OverwriteWithZero(uint64_t start)
{
	auto zeroReWrite = [](uint8_t* data, uint32_t size) -> void
	{
		for (uint32_t i = 0; i < size; i++)
		{
			data[i] = 0x00;
		}
	};

	Overwrite(start, zeroReWrite);
}

void Removal::Proection::OverwriteWithFF(uint64_t start)
{
	auto zeroReWrite = [](uint8_t* data, uint32_t size) -> void
	{
		for (uint32_t i = 0; i < size; i++)
		{
			data[i] = 0xFF;
		}
	};

	Overwrite(start, zeroReWrite);	
}

void Removal::Proection::OverwriteWithRandom(uint64_t start)
{
	auto zeroReWrite = [](uint8_t* data, uint32_t size) -> void
	{
		auto random = RandomGenerator::CreateRandomData(size);
		for (uint32_t i = 0; i < size; i++)
		{
			data[i] = random[i];
		}
	};

	Overwrite(start, zeroReWrite);	
}

void Removal::Proection::Overwrite(uint64_t start,
	std::function<void(uint8_t* data, uint32_t size)> overwriteimpl)
{
	auto high = static_cast<DWORD>((start >> 32) & 0xFFFFFFFFul);
	auto low = static_cast<DWORD>(start & 0xFFFFFFFFul);
#ifdef _DEBUG
	std::cout << "High: " << std::hex << high << ", low: " << low << std::dec << std::endl;
#endif

	uint64_t Size;

	if ((Size = stop.GetPosition() - start) > GetPageSize())
	{
		Size = GetPageSize();
	}

#ifdef _DEBUG
	std::cout << "Size: " << std::hex << Size << std::dec << std::endl;
#endif

	auto* data = static_cast<uint8_t*> (::MapViewOfFile(map.GetHandle(),
		FILE_MAP_READ | FILE_MAP_WRITE, high, low, static_cast<SIZE_T> (Size)));
	if (!data)
	{
		throw ErrorInfo::MakeLastError();
	}

	overwriteimpl(data, static_cast<uint32_t>(Size));

	::UnmapViewOfFile(data);
}
