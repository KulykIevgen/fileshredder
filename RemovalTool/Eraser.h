#pragma once
#include <string>
#include <memory>

#include "Proection.h"

namespace Removal
{
	class Eraser
	{
	public:
		Eraser(const std::wstring& name);
		~Eraser() = default;
		void EraseContent();		
		static void Destroy(const std::wstring& name);
		uint64_t GetLocation() const;

	private:
		std::unique_ptr<FileObject> fileObject;
		std::unique_ptr<MapObject> mapObject;
		std::unique_ptr<Proection> proection;
		std::wstring filepath;
		uint64_t sectorClusterOffset;

		uint64_t GetFileLocation(HANDLE fileHandle) const;
		static std::wstring EraseName(const std::wstring& name);
		static void DeleteFromDisk(const std::wstring& name);
	};
}

