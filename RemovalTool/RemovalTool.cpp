﻿#include <iostream>
#include "Eraser.h"

int main(int argc,char* argv[])
{
	if (argc == 2)
	{
		std::string name = argv[1];
		std::wstring UnicodeName(name.cbegin(), name.cend());
		Removal::Eraser::Destroy(UnicodeName);
	}
	else
	{
		std::cout << "Bad usage\nType 'RemovalTool.exe filename'" << std::endl;
	}
	return 0;
}

