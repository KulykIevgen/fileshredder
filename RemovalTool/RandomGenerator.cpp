#define WIN32_NO_STATUS
#include <windows.h>
#undef WIN32_NO_STATUS

#include "RandomGenerator.h"
#include "ErrorInfo.h"
#include <bcrypt.h>
#include <ntstatus.h>
#include <winternl.h>
#pragma comment(lib,"Bcrypt.lib")

#ifndef NT_SUCCESS
#define NT_SUCCESS(Status) ((NTSTATUS)(Status) >= 0)
#endif

typedef ULONG(WINAPI* NtStatusToDosError)(NTSTATUS);

std::vector<uint8_t> RandomGenerator::CreateRandomData(uint32_t size)
{
	std::vector<uint8_t> result(size, 0);

	NTSTATUS status = BCryptGenRandom(NULL, result.data(),
		static_cast<ULONG> (result.size()), BCRYPT_USE_SYSTEM_PREFERRED_RNG);

	if (!NT_SUCCESS(status))
	{
		auto&& convert = reinterpret_cast<NtStatusToDosError> (GetProcAddress(
			GetModuleHandle(L"ntdll.dll"),"RtlNtStatusToDosError"));
		if (!convert)
		{
			throw ErrorInfo::MakeLastError();
		}		
		throw ErrorInfo::MakeErrorFromCode(convert(status));
	}

	return std::forward<std::vector<uint8_t>>(result);
}
