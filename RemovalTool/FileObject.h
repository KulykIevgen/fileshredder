#pragma once
#include <string>
#include <Windows.h>

namespace Removal
{
	class FileObject
	{
	public:
		explicit FileObject(const std::wstring& name);
		~FileObject();
		HANDLE GetHandle() const noexcept;
		FileObject() = delete;
		FileObject(const FileObject&) = delete;
		FileObject(FileObject&&) = delete;
		FileObject& operator=(const FileObject&) = delete;
		FileObject& operator=(FileObject&&) = delete;

	private:
		HANDLE fileHandle;
	};

}

