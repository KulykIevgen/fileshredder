#pragma once
#include "FileObject.h"

namespace Removal
{
	class MapObject
	{
	public:
		explicit MapObject(const FileObject& FileHandle);
		~MapObject();
		MapObject() = delete;
		MapObject(const MapObject&) = delete;
		MapObject(MapObject&&) = delete;
		MapObject& operator=(const MapObject&) = delete;
		MapObject& operator=(MapObject&&) = delete;
		HANDLE GetHandle() const noexcept;
		uint64_t GetSize() const noexcept;

	private:
		HANDLE MapHandle;
		uint64_t fileparts;
	};
}

